package samples.monitor;
import java.util.Scanner;

// спробувати забрати всі блоки синхронізації і виклик методів wait() i notify()
// тоді буде некоректний результат
class Payment {
    private int amount;
    private boolean close;
    public int getAmount() {
        return amount;
    }
    public boolean isClose() {
        return close;
    }
    public synchronized void doPayment() {
        try {
            System.out.println("Start payment:");
            while (amount <= 0) {
                this.wait(); //зупинка потоку і звільнення блокування
// після повернення блокування виконання буде продовжено
            }
// код виконання платежу
            close = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Payment is closed : " + close);
    }
    public void initAmount() {
        Scanner scan = new Scanner(System.in);
        amount = scan.nextInt();
    }
}
public class PaymentRunner {
    public static void main(String[] args) throws InterruptedException {
        final Payment payment = new Payment();
        new Thread() {
            public void run() {
                payment.doPayment(); // виклик synchronized методу
            }
        }.start();
        Thread.sleep(200);
        synchronized (payment) { // 1-й блок
            System.out.println("Init amount:");
            payment.initAmount();
            payment.notify(); // повідомлення про повернення блокування
        }
        synchronized (payment) { // 2-й блок

            payment.wait(1_000);
            System.out.println("ok");
        }
    }
}