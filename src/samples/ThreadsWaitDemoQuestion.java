package samples;

/**
 * Created by andriy on 5/14/17.
 */
public class ThreadsWaitDemoQuestion {
    public static void main(String[] args) throws InterruptedException {

        // What will be the output?
        Thread thread = new Thread(() -> System.out.print("Thread end."));
        synchronized (thread) {
            thread.start();
            thread.wait();
        }
        System.out.print("Main end");

        // #1 Thread end. and hang-up
        // #2 Thread end. Main end  <= correct
        // #3 Result is unpredictable

        // join() is implemented in this way
        // When thread finished execution, JVM call notifyAll() to this thread
        // thats why thread will exited from this synchronization section
        // end sop will be executed
        // recommendation: never synchronized by thread
    }
}
