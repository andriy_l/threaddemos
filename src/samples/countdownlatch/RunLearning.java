package samples.countdownlatch;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.Random;
/*
Студенту пропонується
для виконання набір завдань. Він виконує їх і переходить в режим очікування оцінок за всі завданням,
 щоб обчислити середнє значення оцінки.
Викладач (Tutor) перевіряє завдання і після кожного перевіреного завдання зрушує лічильник на одиницю.
 Коли всі завдання студента перевірені,
лічильник стає рівним нулю і бар'єр знімається, проводяться необхідні обчислення в класі Student.
 */

class Student extends Thread {
    private Integer idStudent;
    private List<Task> taskList;
    private CountDownLatch countDown;

    public Student(Integer idStudent, int numberTasks) {
        this.idStudent = idStudent;
        this.countDown = new CountDownLatch(numberTasks);
        this.taskList = new ArrayList<Task>(numberTasks);
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    public CountDownLatch getCountDownLatch() {
        return countDown;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void addTask(Task task) {
        taskList.add(task);
    }

    public void run() {
        int i = 0;
        for (Task inWork : taskList) {
            // на виконання завдання потрібний певний час
            try {
                Thread.sleep(new Random().nextInt(100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // відправка відповіді

            inWork.setAnswer("Answer #" + ++i);
            System.out.println("Answer #" + i + " from " + idStudent);
        }
        try {
            countDown.await(); // очікування перевірки завдань
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // підрахунок оцінки за всі задачі
        float averageMark = 0;
        for (Task inWork : taskList) {
            // виконання завдання
            averageMark += inWork.getMark(); // відправка відповіді
        }

        averageMark /= taskList.size();
        System.out.println("Student " + idStudent + ": Average mark = "
                + averageMark);
    }
}

class Tutor extends Thread {
    private Integer idTutor;
    private List<Student> list;

    public Tutor() {
        this.list = new ArrayList<>();
    }

    public Tutor(List<Student> list) {
        this.list = list;
    }

    public Integer getIdTutor() {
        return idTutor;
    }

    public void setIdTutor(Integer id) {
        this.idTutor = id;
    }

    public void run() {
        for (Student st : list) {
            // перевірити чи видані студенту завдання
            List<Task> tasks = st.getTaskList();
            for (Task current : tasks) {
                // перевірити наявність відповіді
                int mark = 3 + new Random().nextInt(7);
                current.setMark(mark);
                System.out.println(mark + " for student N "
                        + st.getIdStudent());
                st.getCountDownLatch().countDown();
            }
            System.out.println("All estimates made for " + st.getIdStudent());
        }
    }
}

class Task {
    private String content;
    private String answer;
    private int mark;

    public Task(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}

public class RunLearning {
    public static void main(String[] args) {
        final int NUMBER_TASKS_1 = 5;

        Student student1 = new Student(322801, NUMBER_TASKS_1);
        for (int i = 0; i < NUMBER_TASKS_1; i++) {
            Task t = new Task("Task #" + i);
            student1.addTask(t);
        }
        final int NUMBER_TASKS_2 = 4;

        Student student2 = new Student(322924, NUMBER_TASKS_2);
        for (int i = 0; i < NUMBER_TASKS_2; i++) {
            Task t = new Task("Task ##" + i);
            student2.addTask(t);
        }

        ArrayList<Student> lst = new ArrayList<Student>();
        lst.add(student1);
        lst.add(student2);

        Tutor tutor = new Tutor(lst);
        student1.start();
        student2.start();
        try { // потік перевірки з затримкою
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        tutor.start();
    }
}