package samples;

public class InterruptDemoThread extends Thread {

    @Override
    public void run() {
        int i = 0;
        while (!isInterrupted()) {
            System.out.println("Thread:" + getName() + " i=" + i++);
            try {
                sleep(1_000);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public static void main(String[] args) {
        InterruptDemoThread th1 = new InterruptDemoThread();
        th1.start();
        try {
            System.out.println("main sleep for 5000");
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        th1.interrupt();
    }
}