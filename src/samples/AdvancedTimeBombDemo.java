package samples;


class TimeBomb implements Runnable{
    int time = 10;
    public TimeBomb(String name) {
        this.name = name;
    }

    private static Thread bombThread;
    private String name;

    public static void setBombTread(Thread bomb){
        TimeBomb.bombThread = bomb;
    }

    @Override
    public void run() {
        if(name.equals("bomb")) {
            Thread thisThread = Thread.currentThread();
            TimeBomb.setBombTread(thisThread);
            for (int i = time; i >= 0; i--) {
                if(thisThread.isInterrupted()){
                    System.out.println("Bomb deactivated");
                    return;
                }
                System.out.println(name + " " + i);

//                try {
//                    Thread.sleep(1_000);
//                } catch (InterruptedException e) {
//                    System.out.println("Bomb deactivated"+e);
//                    return;
//                }
            }
            System.out.println("Boom!");
        }else{
            for (int i = time; i >= 0; i--) {
                System.out.println(name + " " + i);
                if(i==5){
                    TimeBomb.bombThread.interrupt();
                    return;
                }
                try {
                    Thread.sleep(1_000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}

public class AdvancedTimeBombDemo {
    public static void main(String[] args) {
        TimeBomb timeBomb = new TimeBomb("bomb");
        TimeBomb antiBomb = new TimeBomb("antibomb");

        Thread timeBombThread = new Thread(timeBomb);
        Thread antiTimeBombThread = new Thread(antiBomb);
        timeBombThread.start();
        antiTimeBombThread.start();

    }
}
