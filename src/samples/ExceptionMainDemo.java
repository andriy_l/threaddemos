package samples;

class SimpleThread2 extends Thread {
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.err.print(e);
        }
        System.out.println("end of SimpleThread2");
    }
}
public class ExceptionMainDemo {
    public static void main(String[ ] args) {
        new SimpleThread2().start();
        System.out.println("end of main with exception");
        throw new RuntimeException();
    }
}
