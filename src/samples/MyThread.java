package samples;

public class MyThread extends Thread {
    @Override
    public void run(){
        String name = Thread. currentThread ().getName();
        for (;;) {
            System.out.print(name + " " );
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
