package samples.semaphore;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

class Cart {
    private AtomicInteger weight = new AtomicInteger(0);

    public void addWeight() {
        weight.getAndIncrement();
    }

    public void reduceWeight() {
        weight.getAndDecrement();
    }

    public int getWeight() {
        return weight.intValue();
    }
}

class AddWorker extends Thread {
    private Cart cart;
    private Semaphore semaphore;
    private String workerName;

    public AddWorker(String name, Cart cart, Semaphore semaphore) {
        this.cart = cart;
        this.semaphore = semaphore;
        this.workerName = name;
    }

    @Override
    public void run() {
        System.out.println(workerName + " started working...");
        try {
            System.out.println(workerName + " waiting for cart...");
            semaphore.acquire();
            System.out.println(workerName + " got access to cart...");
            for (int i = 0; i < 3; i++) {
                cart.addWeight();
                System.out.println(workerName + " changed weight to: "
                        + cart.getWeight());
                Thread.sleep(1000);
            }
            semaphore.release();
            System.out.println(workerName + " finished working with cart...");
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }
}

class ReduceWorker extends Thread {
    private Cart cart;
    private Semaphore semaphore;
    private String workerName;

    public ReduceWorker(String name, Cart cart, Semaphore semaphore) {
        this.cart = cart;
        this.semaphore = semaphore;
        this.workerName = name;
    }

    @Override
    public void run() {
        System.out.println(workerName + " started working...");
        try {
            System.out.println(workerName + " waiting for cart...");
            semaphore.acquire(2);
            System.out.println(workerName + " got access to cart...");
            int weight = cart.getWeight();
            for (int i = 0; i < weight; i++) {
                cart.reduceWeight();
                System.out.println(workerName + " changed weight to: " + cart.getWeight());
                Thread.sleep(1000);
            }
            semaphore.release(2);
            System.out.println(workerName + " finished working with cart...");
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }
}

public class SemaphoreExample {
    public static void main(String[] a) {
        Semaphore semaphore = new Semaphore(2);
        Cart cart = new Cart();
        new AddWorker("Adder1", cart, semaphore).start();
        new AddWorker("Adder2", cart, semaphore).start();
        new ReduceWorker("Reducer", cart, semaphore).start();
    }
}
