package samples.synchronize.syncmethod;

import java.util.concurrent.Callable;
/*
If you declare the method as synchonized (public synchronized int getA()) you synchronize on the whole object,
so two thread accessing a different variable from this same object would block each other anyway.

If you want to synchronize only on one variable at a time,
so two threads won't block each other while accessing different variables,
you have synchronize on them separately in synchronized () blocks.
But we can syncronize() only non-primitive variables
 */
class X {

    private int a;
    private int b;

    public synchronized int getA(String str){
        System.out.println("Now i am running by " + str);
        a++;
        return a;
    }

    public synchronized int getB(String str){
        System.out.println("Now i am running by " + str);
        b++;
        return b;
    }

}

class XRunnerA extends Thread{
    private final static int PAUSE = 10_000;
    private X x;
    private String name;
    XRunnerA(X x, String name){
        this.x = x;
        this.name = name;
    }
    @Override
    public void run(){
        while (true) {
            Thread.currentThread().setName(name);
            // obj - object is locked
            // call obj.wait() and if this call throws an exception it means thread in Java is not holding the lock,
            // otherwise thread holds the lock.
            //

            try {
                try {
                    x.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }catch (IllegalMonitorStateException e) {
                System.out.println(name + " x is unlocked");
            }
                    System.out.println("Result from getA: " + x.getA(name));
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }

class XRunnerB extends Thread{
    private final static int PAUSE = 10_000;
    private X x;
    private String name;
    XRunnerB(X x, String name){
        this.x = x;
        this.name = name;
    }
    @Override
    public void run(){
        while (true) {
            Thread.currentThread().setName(name);
            if(holdsLock(x)){
                System.out.println(" x is locked ");
            }
            System.out.println("Result from getB: " + x.getB(name));
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


public class SyncMethodDemo{
    public static void main(String[] args) {
        X x = new X();
        XRunnerA xRunnerA = new XRunnerA(x,"A");
        XRunnerB xRunnerB = new XRunnerB(x,"B");
        xRunnerA.start();
        xRunnerB.start();
    }

}
