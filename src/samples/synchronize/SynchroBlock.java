package samples.synchronize;

public class SynchroBlock {
    public static int counter = 0;
    public static void main(String args[ ]) {
        //final StringBuilder s = new StringBuilder();
        final StringBuffer s = new StringBuffer();
        new Thread() {
            public void run() {
                synchronized (s) {
                    do {
                        s.append("A");
                        System.out.println(s);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            System.err.print(e);
                        }
                    } while (SynchroBlock.counter++ < 2);
                } // end of synchronized
            }
        }.start();
        new Thread() {
            public void run() {
                synchronized (s) {
                    while (SynchroBlock.counter++ < 6) {
                        s.append("B");
                        System.out.println(s);
                    }
                } // end of synchronized
            }
        }.start();
    }
}
