package samples.synchronize;

public class BufferThread {
    static int counter = 0;
    static StringBuffer s = new StringBuffer(); // switch to StringBuilder
    // "thread-safe" StringBuffer
    // StringBuffer blocked by another thread cause stop on blocked object
    // if use StringBuilder this problem will not happen
    public static void main(String args[ ]) {

        new Thread() {
            public void run() {
                synchronized (s) {
                    while (BufferThread.counter++ < 3) {
                        s.append("A");
                        System.out.print("> " + counter + " ");
                        System.out.println(s);
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } // end synchronized-block
            }
        }.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (BufferThread.counter++ < 6) {
            System.out.print("< " + counter + " ");
            // in this place thread main will wait for freeing object s
            s.append("B");
            System.out.println(s);
        }
    }
}
