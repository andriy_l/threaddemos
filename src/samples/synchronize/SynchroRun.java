package samples.synchronize;
import java.io.*;
class Resource {
    private FileWriter fileWriter;
    public Resource (String file) throws IOException {
        // check for file availability
        fileWriter = new FileWriter(file, true);
    }
    public void writing(String str, int i) {

   // public synchronized void writing(String str, int i) {
        try {
            fileWriter.append(str + i);
            System.out.print(str + i);
            Thread.sleep((long)(Math.random() * 50));
            fileWriter.append("->" + i + " ");
            System.out.print("->" + i + " ");
        } catch (IOException e) {
            System.err.print("file error: " + e);
        } catch (InterruptedException e) {
            System.err.print("error stream: " + e);
        }
    }
    public void close() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            System.err.print("error of closing file: " + e);
        }
    }
}

class SyncThread extends Thread {
    private Resource rs;
    public SyncThread(String name, Resource rs) {
        super(name);
        this.rs = rs;
    }
    public void run() {
        for (int i = 0; i < 5; i++) {
            rs.writing(getName(), i); // place of synchronization
        }
    }
}
public class SynchroRun {
    public static void main(String[ ] args) {

        Resource s = null;
        try {
            s = new Resource ("data"+File.separator+"result.txt");
            SyncThread t1 = new SyncThread("First", s);
            SyncThread t2 = new SyncThread("Second", s);
            t1.start();
            t2.start();
            t1.join();
            t2.join();
        } catch (IOException e) {
            System.err.print("file error: " + e);
        } catch (InterruptedException e) {
            System.err.print("error stream: " + e);
        } finally {
            s.close();
        }
    }
}
