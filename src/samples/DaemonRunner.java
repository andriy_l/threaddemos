package samples;

class SimpleThread extends Thread {
    public void run() {
        try {
            if (isDaemon()) {
                System.out.println("Start of daemon thread");
                Thread.sleep(10_000); // change to  1
                // Thread.sleep(1); // change to  1
                // by default daemon was too late :) and main-thread finished it
            } else {
                System.out.println("Start of ordinary thread");
                // Thread.yield(); //if 1 do not help
            }
        } catch (InterruptedException e) {
            System.err.print(e);
        } finally {
            if (!isDaemon()) {
                System.out.println("Finish of ordinary thread");
            } else {
                System.out.println("Finish of thread-daemon");
            }
        }
    }
}
public class DaemonRunner {
    public static void main(String[ ] args) {

        SimpleThread usual = new SimpleThread();

        SimpleThread daemon = new SimpleThread();
        daemon.setDaemon(true);
        daemon.start();
        usual.start();
        System.out.println("last main");
    }
}
