package samples;

public class YieldRunner {
    public static void main(String[] args) {
        new Thread() { // anon class
            public void run() {
                System.out.println("Start of thread 1");
                Thread.yield();
                System.out.println("Finish of thread  1");
            }
        }.start(); // thread start
        new Thread() {
            public void run() {
                System.out.println("Start of thread 2");
                System.out.println("Finish of thread  2");
            }
        }.start();


    }
}
