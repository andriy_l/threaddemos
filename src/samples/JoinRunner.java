package samples;

class JoinThread extends Thread {
    public JoinThread (String name) {
        super(name);
    }
    public void run() {
        String nameT = getName();
        long timeout = 0;
        System.out.println("Start of thread  " + nameT);
        try {
            switch (nameT) {
                case "First":
                    timeout = 5_000;
                    break;
                case "Second":
                    timeout = 1_000;
            }
            Thread.sleep(timeout);
            System.out.println("Finish of thread  " + nameT);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
public class JoinRunner {
    static {
        System.out.println("Start of thread main");
    }
    public static void main(String[ ] args) {
        JoinThread t1 = new JoinThread("First");
        JoinThread t2 = new JoinThread("Second");
        t1.start();
        t2.start();
        try {
            t1.join(); // thread main stoppeed until thread t1 finished it's work
            // if join(timeout) - main will be stopped until timeout finished
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()); // name of current thread
    }
}
