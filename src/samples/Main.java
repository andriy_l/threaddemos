package samples;

import java.lang.management.ManagementFactory;
import java.util.*;

public class Main {
static int a;
    public static void main(String[] args) {
        System.out.println(a);
        /*
        Thread thread = Thread.currentThread();
        String name = thread.getName();
      System.out.println(name + " method start");

        System.out.println("Thread:" + " " + thread.getName()+ " with id:" + thread.getId()+" prio: "+thread.getPriority()+" state:" + thread.getState());
        MyRunnable myRunnable = new MyRunnable();
        Thread child1 = new Thread(myRunnable);
        System.out.println(name + " start thread " + child1.getName());
        MyThread child2 = new MyThread();
        child1.start();
        if(child1.getState()== Thread.State.BLOCKED)
        child1.start();
        System.out.println(name + " start thread " + child2.getName());
*/
        Runnable r1 = () -> {
            // PID of process in OS. Format PID@hostname
            String nameInOS = ManagementFactory.getRuntimeMXBean().getName();
            String nameInJVM = Thread.currentThread().getName();
           // System.out.println("I'm thread inside JVM " + nameInJVM + ", in OS i'm " + nameInOS);
            for (int i = 0; i < 10; i++) {
                System.out.println(nameInOS+ " "+ nameInJVM +" Eating");
                try {
                    Thread.sleep(7); // остановка на 7 миллисекунд
                } catch (InterruptedException e) {
                    System.err.print(e);
                }
            }
        };

        System.out.println("I'm parrent process: " + Thread.currentThread().getName());
        Runnable r2 = new MThread2();
        Thread t1 = new MThread1(); // run through Thread
        Thread t2 = new Thread(r1); // run through Runnable
        Thread t3 = new Thread(r1); // run through Runnable
        //t1.start();
        t2.start();
        t3.start();
        while (true) {
            if (t2.getState() == Thread.State.TERMINATED & t3.getState() == Thread.State.TERMINATED) {
                System.out.println("Threads completed");
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
        }


    }

}
class MThread1 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 500; i++) {
            System.out.println(this.getName()+" Talking");
            try {
                Thread.sleep(700); // остановка на 7 миллисекунд
            } catch (InterruptedException e) {
                System.err.print(e);
            }
        }
    }
}
class MThread2 implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 500; i++) {
            System.out.println(Thread.currentThread().getName()+" Walking");
            try {
                Thread.sleep(700);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
    }
}