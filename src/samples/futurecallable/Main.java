package samples.futurecallable;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created by andriy on 2/8/17.
 */
class Result implements Callable<Integer>{
    int value = 1000;

    @Override
    public Integer call(){
        value = new Random().nextInt(value);
        return new Integer(value + value + value);
    }
}
public class Main {
    public static void main(String[] args) {
        Thread t = new Thread();

    }

}
