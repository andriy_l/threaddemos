package testforkjoin;


import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

class SumElements extends RecursiveTask<Integer>{
    private static final int THRESHOLD = 20;
    private int from;
    private int to;
    private int[] array;

    SumElements(int from, int to, int[] array){
        this.from = from;
        this.to = to;
        this.array = array;
    }

    @Override
    protected Integer compute() {

        if((to - from) < THRESHOLD ){
            int result = 0;
            for(int i = from; i < to; i++){
                result += array[i];
            }
            return result;
        } else {
            int mid = (from + to) / 2;
            SumElements sum1 = new SumElements(from, mid, array);
            SumElements sum2 = new SumElements(mid, to, array);
            invokeAll(sum1,sum2);
            return sum1.join() + sum2.join();
        }
    }
}

public class ForkJoinProblem {
    public static final int ARRAY_SIZE = 1_000_000;
    public static void main(String[] args) {
        int[] array = new int[ARRAY_SIZE];
        Random r = new Random();
        for (int i = 0; i < array.length; i++){
            array[i] = r.nextInt(100);
        }

        SumElements sumElements = new SumElements(0, array.length, array);
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        forkJoinPool.invoke(sumElements);
        System.out.println("Result: " + sumElements.join());
        System.out.println("get parallelizm level of this pool: " + forkJoinPool.getParallelism());
        System.out.println("Pool size: " + forkJoinPool.getPoolSize());
        System.out.println("active thread count: " + forkJoinPool.getActiveThreadCount());

    }
}
